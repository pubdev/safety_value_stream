import 'package:rxdart/rxdart.dart';

class CombineLatestValueStream<T, R> extends CombineLatestStream<T, R>
    implements ValueStream<R> {
  final Iterable<ValueStream<T>> _streams;
  final R Function(List<T> values) _combiner;

  CombineLatestValueStream(
    this._streams,
    this._combiner,
  ) : super(
          _streams,
          _combiner,
        );

  /// see [CombineLatestStream.list]
  static CombineLatestValueStream<T, List<T>> list<T>(
    Iterable<ValueStream<T>> streams,
  ) =>
      CombineLatestValueStream<T, List<T>>(
        streams,
        (List<T> values) => values,
      );

  /// see [CombineLatestStream.combine2]
  static CombineLatestValueStream<Object?, R> combine2<A, B, R>(
    ValueStream<A> streamOne,
    ValueStream<B> streamTwo,
    R Function(A a, B b) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamOne, streamTwo],
        (List<Object?> values) => combiner(values[0] as A, values[1] as B),
      );

  /// see [CombineLatestStream.combine3]
  static CombineLatestValueStream<Object?, R> combine3<A, B, C, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    R Function(A a, B b, C c) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamA, streamB, streamC],
        (List<Object?> values) {
          return combiner(
            values[0] as A,
            values[1] as B,
            values[2] as C,
          );
        },
      );

  /// see [CombineLatestStream.combine4]
  static CombineLatestValueStream<Object?, R> combine4<A, B, C, D, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    R Function(A a, B b, C c, D d) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamA, streamB, streamC, streamD],
        (List<Object?> values) {
          return combiner(
            values[0] as A,
            values[1] as B,
            values[2] as C,
            values[3] as D,
          );
        },
      );

  /// see [CombineLatestStream.combine5]
  static CombineLatestValueStream<Object?, R> combine5<A, B, C, D, E, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    ValueStream<E> streamE,
    R Function(A a, B b, C c, D d, E e) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamA, streamB, streamC, streamD, streamE],
        (List<Object?> values) {
          return combiner(
            values[0] as A,
            values[1] as B,
            values[2] as C,
            values[3] as D,
            values[4] as E,
          );
        },
      );

  /// see [CombineLatestStream.combine6]
  static CombineLatestValueStream<Object?, R> combine6<A, B, C, D, E, F, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    ValueStream<E> streamE,
    ValueStream<F> streamF,
    R Function(A a, B b, C c, D d, E e, F f) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamA, streamB, streamC, streamD, streamE, streamF],
        (List<Object?> values) {
          return combiner(
            values[0] as A,
            values[1] as B,
            values[2] as C,
            values[3] as D,
            values[4] as E,
            values[5] as F,
          );
        },
      );

  /// see [CombineLatestStream.combine7]
  static CombineLatestValueStream<Object?, R> combine7<A, B, C, D, E, F, G, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    ValueStream<E> streamE,
    ValueStream<F> streamF,
    ValueStream<G> streamG,
    R Function(A a, B b, C c, D d, E e, F f, G g) combiner,
  ) =>
      CombineLatestValueStream<Object?, R>(
        [streamA, streamB, streamC, streamD, streamE, streamF, streamG],
        (List<Object?> values) {
          return combiner(
            values[0] as A,
            values[1] as B,
            values[2] as C,
            values[3] as D,
            values[4] as E,
            values[5] as F,
            values[6] as G,
          );
        },
      );

  /// see [CombineLatestStream.combine8]
  static CombineLatestValueStream<Object?, R>
      combine8<A, B, C, D, E, F, G, H, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    ValueStream<E> streamE,
    ValueStream<F> streamF,
    ValueStream<G> streamG,
    ValueStream<H> streamH,
    R Function(A a, B b, C c, D d, E e, F f, G g, H h) combiner,
  ) =>
          CombineLatestValueStream<Object?, R>(
            [
              streamA,
              streamB,
              streamC,
              streamD,
              streamE,
              streamF,
              streamG,
              streamH
            ],
            (List<Object?> values) {
              return combiner(
                values[0] as A,
                values[1] as B,
                values[2] as C,
                values[3] as D,
                values[4] as E,
                values[5] as F,
                values[6] as G,
                values[7] as H,
              );
            },
          );

  /// see [CombineLatestStream.combine9]
  static CombineLatestValueStream<Object?, R>
      combine9<A, B, C, D, E, F, G, H, I, R>(
    ValueStream<A> streamA,
    ValueStream<B> streamB,
    ValueStream<C> streamC,
    ValueStream<D> streamD,
    ValueStream<E> streamE,
    ValueStream<F> streamF,
    ValueStream<G> streamG,
    ValueStream<H> streamH,
    ValueStream<I> streamI,
    R Function(A a, B b, C c, D d, E e, F f, G g, H h, I i) combiner,
  ) =>
          CombineLatestValueStream<Object?, R>(
            [
              streamA,
              streamB,
              streamC,
              streamD,
              streamE,
              streamF,
              streamG,
              streamH,
              streamI
            ],
            (List<Object?> values) {
              return combiner(
                values[0] as A,
                values[1] as B,
                values[2] as C,
                values[3] as D,
                values[4] as E,
                values[5] as F,
                values[6] as G,
                values[7] as H,
                values[8] as I,
              );
            },
          );

  @override
  Object get error {
    final error = errorOrNull;
    if (error == null) {
      throw ValueStreamError.hasNoError();
    }
    return error;
  }

  @override
  Object? get errorOrNull {
    final errors = <Object>[];
    for (final stream in _streams) {
      final error = stream.errorOrNull;
      if (error != null) {
        errors.add(error);
      }
    }
    if (errors.isEmpty) {
      return null;
    }
    if (errors.length == 1) {
      return errors.first;
    }
    return CombineLatestValueStreamError(errors);
  }

  @override
  bool get hasError => _streams.map((e) => e.hasError).contains(true);

  @override
  bool get hasValue => !_streams.map((e) => e.hasValue).contains(false);

  @override
  StackTrace? get stackTrace {
    final stackTraces = _streams
        .map((e) => e.stackTrace)
        .where((element) => element != null)
        .cast<StackTrace>()
        .toList();
    if (stackTraces.isEmpty) {
      return null;
    }
    if (stackTraces.length == 1) {
      return stackTraces.first;
    }
    return CombineLatestValueStreamStackTrace(stackTraces);
  }

  @override
  R get value {
    final list = <T>[];
    for (final stream in _streams) {
      final value = stream.value;
      list.add(value);
    }
    return _combiner(list);
  }

  @override
  R? get valueOrNull {
    final list = <T>[];
    for (final stream in _streams) {
      if (stream.hasValue) {
        final value = stream.value;
        list.add(value);
      } else {
        return null;
      }
    }
    return _combiner(list);
  }
}

class CombineLatestValueStreamError {
  final List<Object> errors;

  CombineLatestValueStreamError(this.errors);

  @override
  String toString() {
    return "$CombineLatestValueStreamError(\n${errors.map((e) => "\t$e").join(',\n')}\n)";
  }
}

class CombineLatestValueStreamStackTrace extends StackTrace {
  final List<StackTrace> stackTraces;

  CombineLatestValueStreamStackTrace(this.stackTraces);

  @override
  String toString() {
    return "$CombineLatestValueStreamError(\n${stackTraces.map((e) => "\t${e.toString().replaceAll('\n', '\n\t')}").join(',\n\n')}\n)";
  }
}
