import 'package:rxdart/rxdart.dart';
import 'package:safety_value_stream/src/utils/value_stream_mapper_wrapper.dart';

extension ValueStreamExtension<T> on ValueStream<T> {
  /// VSE - ValueStreamExtension
  VSE<T> get vse => VSE<T>(this);
}

class VSE<T> {
  final ValueStream<T> _valueStream;

  VSE(this._valueStream);

  /// utils
  ValueStream<T> distinct() => custom(
        (stream) => stream.distinct(),
      );

  ValueStream<S> map<S>(S Function(T event) convert) => customMapper(
        convert,
        (map, stream) => stream.map(map),
      );

  ValueStream<S> cast<S>() => customMapper(
        (a) => a as S,
        (map, stream) => stream.cast<S>(),
      );

  /// base
  ValueStream<T> custom(Stream<T> Function(ValueStream<T>) pipe) =>
      ValueStreamMapperWrapper(
        _valueStream,
        (v) => v,
        pipe,
      );

  ValueStream<R> customMapper<R>(
    R Function(T event) mapper,
    Stream<R> Function(
      R Function(T event) mapper,
      ValueStream<T> stream,
    ) pipe,
  ) =>
      ValueStreamMapperWrapper<R, T>(
        _valueStream,
        mapper,
        (stream) => pipe(mapper, stream),
      );
}
