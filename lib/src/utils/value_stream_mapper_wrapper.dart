import 'dart:async';

import 'package:rxdart/rxdart.dart';

class ValueStreamMapperWrapper<R, T> extends StreamView<R>
    implements ValueStream<R> {
  final R Function(T event) _mapper;
  final ValueStream<T> _source;

  ValueStreamMapperWrapper(
    this._source,
    this._mapper,
    Stream<R> Function(ValueStream<T>) streamMapper,
  ) : super(streamMapper(_source));

  @override
  Object get error => _source.error;

  @override
  Object? get errorOrNull => _source.errorOrNull;

  @override
  bool get hasError => _source.hasError;

  @override
  bool get hasValue => _source.hasValue;

  @override
  StackTrace? get stackTrace => _source.stackTrace;

  @override
  R get value => _mapper(_source.value);

  @override
  R? get valueOrNull {
    final value = _source.valueOrNull;
    if (value is T) {
      return _mapper(value);
    }
    return null;
  }
}
