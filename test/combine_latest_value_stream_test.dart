import 'package:flutter_test/flutter_test.dart';
import 'package:rxdart/rxdart.dart';
import 'package:safety_value_stream/safety_value_stream.dart';

main() {
  test('CombineLatestValueStream with value', () {
    final stream1 = BehaviorSubject.seeded(1);
    final stream2 = BehaviorSubject.seeded(2);
    final combineStream =
        CombineLatestValueStream.combine2(stream1, stream2, (a, b) => a + b);
    _expect(
      stream: combineStream,
      value: 3,
    );
  });
  test('CombineLatestValueStream without value', () {
    final stream1 = BehaviorSubject();
    final stream2 = BehaviorSubject.seeded(2);
    final combineStream =
        CombineLatestValueStream.combine2(stream1, stream2, (a, b) => a + b);
    _expect(
      stream: combineStream,
      hasValue: false,
    );
  });
  test('CombineLatestValueStream with error', () {
    const error = 'error';
    final stream1 = BehaviorSubject();
    final stream2 = BehaviorSubject();
    stream1.addError(error);
    final combineStream =
        CombineLatestValueStream.combine2(stream1, stream2, (a, b) => a + b);
    _expect(
      stream: combineStream,
      hasValue: false,
      hasError: true,
      error: error,
    );
  });
  test('CombineLatestValueStream without error', () {
    final stream1 = BehaviorSubject();
    final stream2 = BehaviorSubject();
    final combineStream =
        CombineLatestValueStream.combine2(stream1, stream2, (a, b) => a + b);
    _expect(
      stream: combineStream,
      hasValue: false,
      hasError: false,
    );
  });

  test('CombineLatestValueStream without combined error', () {
    const error = 'error';
    final stream1 = BehaviorSubject();
    final stream2 = BehaviorSubject();
    stream1.addError(error);
    stream2.addError(error);
    final combineStream =
        CombineLatestValueStream.combine2(stream1, stream2, (a, b) => a + b);

    _expect(
      stream: combineStream,
      hasValue: false,
      hasError: true,
      isCombinedError: true,
    );
  });
}

void _expect<T>({
  required CombineLatestValueStream stream,
  bool hasValue = true,
  bool hasError = false,
  bool hasStackTrace = false,
  Object? value,
  Object? error,
  bool isCombinedError = false,
  bool isCombinedStackTrace = false,
}) {
  if (hasValue) {
    expect(stream.value, value);
  } else {
    expect(
      () => stream.value,
      throwsA(
        isInstanceOf<ValueStreamError>(),
      ),
    );
  }
  expect(stream.valueOrNull, value);
  if (!hasError) {
    expect(
      () => stream.error,
      throwsA(
        isInstanceOf<ValueStreamError>(),
      ),
    );
  } else {
    if (isCombinedError) {
      expect(
        stream.error,
        isInstanceOf<CombineLatestValueStreamError>(),
      );
      expect(
        stream.errorOrNull,
        isInstanceOf<CombineLatestValueStreamError>(),
      );
    } else {
      expect(
        stream.error,
        error,
      );
      expect(
        stream.errorOrNull,
        error,
      );
    }
  }
  if (hasStackTrace) {
    if (isCombinedStackTrace) {
      expect(
        stream.stackTrace,
        isInstanceOf<CombineLatestValueStreamStackTrace>(),
      );
    } else {
      expect(stream.stackTrace, isNotNull);
    }
  } else {
    expect(stream.stackTrace, null);
  }
  expect(stream.hasError, hasError);
  expect(stream.hasValue, hasValue);
}
