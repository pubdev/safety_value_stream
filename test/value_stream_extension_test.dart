import 'package:flutter_test/flutter_test.dart';
import 'package:rxdart/rxdart.dart';
import 'package:safety_value_stream/safety_value_stream.dart';

main() {
  test('vse.map value getter test', () {
    var value = 1;
    final stream1 = BehaviorSubject.seeded(value);
    final mapped = stream1.vse.map((event) => event.toString());
    expect(mapped.value, value.toString());
    value = 2;
    stream1.add(value);
    expect(mapped.value, value.toString());
  });

  test('vse.distinct value getter test', () {
    var value = 1;
    final stream1 = BehaviorSubject.seeded(value);
    final mapped = stream1.vse.distinct();
    expect(mapped.value, value);
    value = 2;
    stream1.add(value);
    expect(mapped.value, value);
  });

  test('vse.distinct value getter test', () {
    var value = 1;
    final stream1 = BehaviorSubject.seeded(value);
    final mapped = stream1.vse.cast();
    expect(mapped.value, value);
    value = 2;
    stream1.add(value);
    expect(mapped.value, value);
  });
}
